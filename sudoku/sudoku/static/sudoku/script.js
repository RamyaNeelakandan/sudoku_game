var letters = "ABCDEFGHI".split("");
var numbers = "123456789".split("");

function clearPuzzle() {
  for (l = 0; l < 9; l++) {
    for (n = 0; n < 9; n++) {
      document.getElementById(letters[l] + numbers[n]).value = "";
    }
  }
}

function fillPuzzle(puzzle) {
  var preset = [];

  preset[0] = `
000600400
700003600
000091080
000000000
050180003
000306045
040200060
903000000
020000100`
  clearPuzzle();
  sudoku = preset[puzzle].split("\n");
    for (r = 1; r < 10; r++) {
      var line = sudoku[r].split("");
      for (c = 0; c < 9; c++) {
        // If the number isn't a zero
        if (line[c] != "0") {
          var square = letters[c] + numbers[r-1];
          document.getElementById(square).value = line[c];
        }
      }
    }
}
