from django import template

register = template.Library()

@register.filter(name='letter')
def letter(values, m):
    return values, m

@register.filter(name='number')
def number(values_l, n):
    """Finds the value in a dictionary"""
    values, m = values_l
    if values:
        return values[m + n]
    else:
        return ""
