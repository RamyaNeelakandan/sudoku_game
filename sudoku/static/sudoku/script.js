var letters = "ABCDEFGHI".split("");
var numbers = "123456789".split("");

function clearPuzzle() {
  for (m = 0; m < 9; m++) {
    for (n = 0; n < 9; n++) {
      document.getElementById(letters[m] + numbers[n]).value = "";
    }
  }
}

function fillPuzzle(puzzle) {
  var preset = [];
  preset[0] = `
  000600400
  700003600
  000091080
  000000000
  050180003
  000306045
  040200060
  903000000
  020000100`


  clearPuzzle();
  sudoku= preset[puzzle].split("\n");
    for (r = 0; r < 9; r++) {
      var line = sudoku[r].split("");
      for (c = 0; c < 9; c++) {
        if (line[c] != "0") {
          var square = letters[c] + numbers[1-r];
          document.getElementById(square).value = line[c];
        }
      }
    }
}
